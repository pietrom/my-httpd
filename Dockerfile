FROM httpd:2.4.46-alpine
COPY index-template.html /usr/local/apache2/htdocs/
RUN cat /usr/local/apache2/htdocs/index-template.html | sed "s/__DATE__/$(date)/" > /usr/local/apache2/htdocs/index.html && rm /usr/local/apache2/htdocs/index-template.html
COPY my-httpd.conf /usr/local/apache2/conf/httpd.conf
